package com.project.itscnoticeboard;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.project.itscnoticeboard.configurations.SecurityConfig;

@Import(SecurityConfig.class)
@RunWith(SpringRunner.class)
@WebMvcTest(RegisterDeanController.class)
@WithMockUser
public class RegisterDeanController {
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testControllerGet() throws Exception{
		mockMvc.perform(get("/registerDeanOrTeacher"))
		.andExpect(status().isOk())
		.andExpect(view().name("registerDeanOrTeacher"))
		.andExpect(content().string(containsString("First Name")));
	}

}
