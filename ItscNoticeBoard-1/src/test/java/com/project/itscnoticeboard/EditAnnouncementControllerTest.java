package com.project.itscnoticeboard;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.project.itscnoticeboard.configurations.SecurityConfig;
import com.project.itscnoticeboard.controllers.EditAnnouncementController;
import com.project.itscnoticeboard.services.AnnouncementService;


@RunWith(SpringRunner.class)
@WebMvcTest(EditAnnouncementController.class)
@Import(SecurityConfig.class)

public class EditAnnouncementControllerTest {
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	AnnouncementService announcementService;
	
	
	@Test
	@WithMockUser
	public void testControllerGet() throws Exception{
		mockMvc.perform(get("/editAnnouncements"))
		.andExpect(status().isOk())
		.andExpect(view().name("editAnnouncements"))
		.andExpect(content().string(containsString("Header")));

}

	
	
}
