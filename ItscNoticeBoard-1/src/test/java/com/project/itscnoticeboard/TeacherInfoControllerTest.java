package com.project.itscnoticeboard;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.project.itscnoticeboard.controllers.TeacherInfoContoller;

@RunWith(SpringRunner.class)
@WebMvcTest(TeacherInfoContoller.class)
public class TeacherInfoControllerTest {
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testControllerGet() throws Exception{
		mockMvc.perform(get("/displayTeacherInfo"))
		.andExpect(status().isOk())
		.andExpect(view().name("displayTeacherInfo"))
		.andExpect(content().string(containsString("No.")));
	}
}
