package com.project.itscnoticeboard.domains;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity
@Data
public class Scholarship {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	String universityName;
	
	String description;
	
	String link;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="dean_id")
	private User user;
	

}
