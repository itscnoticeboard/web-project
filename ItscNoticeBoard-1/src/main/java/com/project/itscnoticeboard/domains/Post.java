package com.project.itscnoticeboard.domains;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
@Entity
@Table(name = "posts")
public class Post {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Date createdAt;
	
	@PrePersist
	void placedAt() {
		this.createdAt = new Date();
	}	
	
	@Column(name = "header")
    @NotBlank(message = "Please provide the header")
    private String header;
    
    @Column(name = "description")
    @NotBlank(message = "Please provide the description")
    private String description;
  
	@NotBlank(message="Input a resource path")
	private String resourcePath;
  
	@NotBlank(message="Choose a semester intended for this post")
	private String semester;
  
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="teacher_id")
	private User user;
	
	
}
