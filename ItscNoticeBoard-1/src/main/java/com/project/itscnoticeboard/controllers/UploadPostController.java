package com.project.itscnoticeboard.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UploadPostController {
	@GetMapping("/uploadPost")
	public String uploadPost() {
		return "uploadPost";
	}
	
}
