package com.project.itscnoticeboard.controllers;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.itscnoticeboard.services.AnnouncementService;
import com.project.itscnoticeboard.domains.Announcement;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/announcement")
public class PostAnnouncementController {
	
	@Autowired
	private AnnouncementService announcementService;
	
	@GetMapping
	@ModelAttribute
	public String announcement(Model model,@ModelAttribute("toBeChanged") Announcement a,RedirectAttributes redirect) {
		if(a.getId() != 0) {
			model.addAttribute("announcement",a);
		}
		else {
			model.addAttribute("announcement", new Announcement());
		}
			return "announcement";
	}
	
	
	@RequestMapping(params="post",method = RequestMethod.POST)
	public String postAnnouncement(@Valid @ModelAttribute("announcement") Announcement announcement, Errors errors,HttpSession session) {
	    
		ServletRequestAttributes requestAttr=(ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		HttpServletRequest request=requestAttr.getRequest();
		session=request.getSession(false);
		
		Long id = (Long) session.getAttribute("editId");
		
		if(errors.hasErrors()) {
			return "announcement";
		}
		
		if(id!=null) {
			
			announcement.setId(id);
			session.invalidate();
		}
		
		announcementService.saveAnnouncement(announcement);
		log.info("Announcement saved");
		
		return "redirect:/announcement";
	}
	
	@RequestMapping(params="clear")
    public String clearAnnouncement() {
    	return "redirect:/announcement";
    }
	
}
