package com.project.itscnoticeboard.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.project.itscnoticeboard.services.UserService;
import com.project.itscnoticeboard.domains.User;






@Controller
@RequestMapping("/displayTeacherInfo")
public class TeacherInfoContoller {
	@Autowired
	UserService userService;
	
	@GetMapping
	public String getTeachers(Model model) {
		
		List<User> teachers=userService.findAllUsersByRole("TEACHER");
		
		model.addAttribute("teachers",teachers);
		return "displayTeacherInfo";
	}
	
	

}
