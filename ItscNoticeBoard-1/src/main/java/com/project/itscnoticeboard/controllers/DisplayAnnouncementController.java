package com.project.itscnoticeboard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.project.itscnoticeboard.services.AnnouncementService;



@Controller 
@RequestMapping("/displayAnnouncement")
public class DisplayAnnouncementController {
	
	@Autowired
	AnnouncementService announcementService;
	
	@GetMapping
	public String displayAnnouncement(Model model) {
		model.addAttribute("announcements",announcementService.findAllAnnouncements());
		return "displayAnnouncement";
	}

}
