package com.project.itscnoticeboard.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.project.itscnoticeboard.services.RoleService;
import com.project.itscnoticeboard.services.UserService;
import com.project.itscnoticeboard.domains.Role;
import com.project.itscnoticeboard.domains.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/registerDeanOrTeacher")
public class RegisterDeanController {
	
  @Autowired
  private UserService userService;
  @Autowired
  private RoleService roleService;
  
  @GetMapping
  public String registerDean(ModelMap model) {
	 
	  
	  model.addAttribute("user",new User());
	  model.addAttribute("role",new Role());
      return "registerDeanOrTeacher";    
  }
  
  @RequestMapping(params="register",method = RequestMethod.POST)
  public String registerUser(@Valid @ModelAttribute("user") User user,BindingResult result_user,@Valid @ModelAttribute("role") Role role,BindingResult result_role,Model model) {
	  
	  if (result_user.hasErrors() || result_role.hasErrors()) {
		 return "registerDeanOrTeacher";
	  }
	  else {
		  userService.saveUser(user);
		  role.setUser(user);
		  roleService.save(role);
		  model.addAttribute("successMessage", "User has been registered successfully");
		  log.info("User and role saved");
	  }
		
	  return "redirect:/registerDeanOrTeacher";
	}
  
  @RequestMapping(params="clear")
  public String clear() {
  	return "redirect:/registerDeanOrTeacher";
  }
}
