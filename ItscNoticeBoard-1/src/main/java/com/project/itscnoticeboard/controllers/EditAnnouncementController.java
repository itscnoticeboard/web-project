package com.project.itscnoticeboard.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.itscnoticeboard.services.AnnouncementService;
import com.project.itscnoticeboard.domains.Announcement;

import lombok.extern.slf4j.Slf4j;



@Slf4j
@Controller
@RequestMapping("/editAnnouncements")

public class EditAnnouncementController {
		
		@Autowired
		AnnouncementService announcementService;
		
		@GetMapping
		public String getAnnouncements(Model model) {
			model.addAttribute("announcements",announcementService.findAllAnnouncements());
			log.info("getting announcements");
			return "editAnnouncements";
		}
		@RequestMapping(params="delete")
		public String deleteAnnouncement(@RequestParam("a_id") Long id) {
			announcementService.deleteAnnouncement(id);
			return "redirect:/editAnnouncements";
		}
		@RequestMapping(params="edit")
		public String editAnnouncement(@RequestParam("a_id") Long id,RedirectAttributes redirect,Model model,HttpSession session) {
			Announcement a=announcementService.findAnnouncement(id).get();
			
			redirect.addFlashAttribute("toBeChanged",a);
			
			ServletRequestAttributes requestAttr=(ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
			HttpServletRequest request=requestAttr.getRequest();
			session=request.getSession();
			session.setAttribute("editId", a.getId());
			
			return "redirect:/announcement";
		}
}
