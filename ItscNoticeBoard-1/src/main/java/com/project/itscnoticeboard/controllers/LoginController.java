package com.project.itscnoticeboard.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {
	@GetMapping("/login")
    public String login(){
		
        return "login";
    }
	@RequestMapping(params="clear",method=RequestMethod.POST)
    public String clearAnnouncement() {
    	return "redirect:/";
    }
	@GetMapping("/accessDenied")
    public String accessDenied(){
        return "accessDenied";
    }
}
