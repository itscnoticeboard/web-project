package com.project.itscnoticeboard.services;

import com.project.itscnoticeboard.domains.Post;

public interface PostService {
	void savePost(Post p);
}
