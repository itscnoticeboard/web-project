package com.project.itscnoticeboard.services;

import java.util.List;
import java.util.Optional;

import com.project.itscnoticeboard.domains.Announcement;


public interface AnnouncementService {
	void deleteAnnouncement(Long id);
	void saveAnnouncement(Announcement a);
	Optional<Announcement> findAnnouncement(Long id);
	List<Announcement> findAllAnnouncements();
}
