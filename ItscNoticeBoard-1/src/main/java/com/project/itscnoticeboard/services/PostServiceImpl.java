package com.project.itscnoticeboard.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.itscnoticeboard.domains.Post;
import com.project.itscnoticeboard.repositories.PostRepository;

@Service
public class PostServiceImpl implements PostService{
	
	@Autowired
	PostRepository postRepository;

	@Override
	public void savePost(Post post) {
		postRepository.save(post);
	}

}
