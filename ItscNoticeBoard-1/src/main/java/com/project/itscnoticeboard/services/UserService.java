package com.project.itscnoticeboard.services;


import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.project.itscnoticeboard.domains.User;


public interface UserService extends UserDetailsService {

	User findUserByUsername(String username);
	void saveUser(User user);
	void saveTeacher(User user);
	List<User> findAllUsersByRole(String role);
	
}