package com.project.itscnoticeboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItscNoticeBoard1Application {

	public static void main(String[] args) {
		SpringApplication.run(ItscNoticeBoard1Application.class, args);
	}

}

