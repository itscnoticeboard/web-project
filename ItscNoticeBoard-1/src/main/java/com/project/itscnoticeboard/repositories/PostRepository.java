package com.project.itscnoticeboard.repositories;



import org.springframework.data.repository.CrudRepository;

import com.project.itscnoticeboard.domains.Post;

public interface PostRepository extends CrudRepository<Post, Long> {

}
