package com.project.itscnoticeboard.repositories;

import org.springframework.data.repository.CrudRepository;

import com.project.itscnoticeboard.domains.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {
	Role findByuserRole(String role);
}
