package com.project.itscnoticeboard.repositories;



import org.springframework.data.repository.CrudRepository;

import com.project.itscnoticeboard.domains.Announcement;

public interface AnnouncementRepository extends CrudRepository<Announcement, Long> {
		
}
