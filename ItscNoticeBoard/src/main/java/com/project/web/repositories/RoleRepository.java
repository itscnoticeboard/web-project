package com.project.web.repositories;

import org.springframework.data.repository.CrudRepository;


import com.project.web.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {
	Role findByuserRole(String role);
}
