package com.project.web;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Entity
@Data
@Table(name="announcements")
public class Announcement {  //JDBC URL- jdbc:h2:mem:testdb
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private Date createdAt;
	
	@PrePersist
	void createdAt() {
		this.createdAt=new Date();
	}
	@NotEmpty(message="Header cannot be empty")
	@NotNull
	private String header;
	
	
	@NotEmpty(message="Description cannot be empty")
	private String detail;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="teacher_id")
	private User user;
}
