package com.project.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;
import com.project.web.Role;


@Data
@Entity
@Table(name = "users")
public class User implements UserDetails{
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
   
    @NotBlank(message = "Please provide a username")
    private String username;
   
    @Size(min = 8, message = "Your password must have at least 8 characters")
    @NotBlank(message = "Please provide your password")
    @NotNull(message="Password doesn't match")
    private String password;
    
   
    @NotNull(message="Password doesn't match")
    @Transient
    private String confirmPassword;
    
    //Setter for password
    public void setPassword(String _password) {
    	this.password=_password;
    }
    
    public void setConfirmPassword(String _confirmPassword) {
    	this.confirmPassword=_confirmPassword;
    	checkPassword();
    }
    
    private void checkPassword() {
    	if(this.password == null || this.confirmPassword == null) 
    		return;
    	else if(!this.password.equals(this.confirmPassword)) {
    		this.password=null;
    	}
    }
   
    @Column(name = "first_name")
    @NotBlank(message = "Please provide your first name")
    private String firstName;
    
    @Column(name = "last_name")
    @NotBlank(message = "Please provide your last name")
    private String lastName;
    
    @Email(message="Invalid Email")
    @NotBlank(message = "Please provide your email")
    private String email;
    
    @NotBlank(message="Course is required")
    private String course;
    
    @NotBlank(message="Office is required")
    private String office;
    
   @Size(min=1,message="You should provide a consultation day")
    private String[] consultationDays;
    
    
    @OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "user")
    private Role role;
    
    @OneToMany(cascade=CascadeType.ALL,
    		   orphanRemoval=true,
    		   mappedBy="user")
    private List<Post> post=new ArrayList<>();
    
    @OneToMany(cascade=CascadeType.ALL,
 		   orphanRemoval=true,
 		   mappedBy="user")
    private List<Announcement> announcement=new ArrayList<>();
    
    @OneToMany(cascade=CascadeType.ALL,
  		   orphanRemoval=true,
  		   mappedBy="user")
     private List<Calender> calender=new ArrayList<>();
    
    @OneToMany(cascade=CascadeType.ALL,
  		   orphanRemoval=true,
  		   mappedBy="user")
     private List<Scholarship> scholarship=new ArrayList<>();

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {

		return Arrays.asList(new SimpleGrantedAuthority(role.getUserRole()));
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
