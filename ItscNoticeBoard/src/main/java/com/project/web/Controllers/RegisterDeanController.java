package com.project.web.Controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.project.web.Role;
import com.project.web.User;
import com.project.web.Services.RoleService;
import com.project.web.Services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/registerDeanOrTeacher")
public class RegisterDeanController {
	
  @Autowired
  private UserService userService;
  @Autowired
  private RoleService roleService;
  
  @GetMapping
  public String registerDean(ModelMap model) {
	  model.addAttribute("user",new User());
	  model.addAttribute("role",new Role());
      return "registerDeanOrTeacher";    
  }
  
  @RequestMapping(params="register",method = RequestMethod.POST)
  public String registerUser(@Valid @ModelAttribute("user") User user,BindingResult result_user,@Valid @ModelAttribute("role") Role role,BindingResult result_role) {
	  if (result_role.hasErrors() || result_role.hasErrors()) {
		 log.info("P: "+user.getConfirmPassword());
		return "registerDeanOrTeacher";
	  }
	  
	  userService.saveUser(user);
	  role.setUser(user);
	  roleService.save(role);
	  log.info("User and role saved");
		
	  return "redirect:/registerDeanOrTeacher";
	}
  
  @RequestMapping(params="clear")
  public String clear() {
  	return "redirect:/registerDeanOrTeacher";
  }
}
