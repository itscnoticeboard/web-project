package com.project.web.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UploadPostController {
	@GetMapping
	public String uploadPost() {
		return "uploadPost";
	}
	
}
