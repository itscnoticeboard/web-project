package com.web.NoticeBoard.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
<<<<<<< HEAD
=======
import org.springframework.security.crypto.password.PasswordEncoder;
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	
	
	  @Override
	  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		  	
		  auth.userDetailsService(userDetailsService)
		  .passwordEncoder(bCryptPasswordEncoder);
	 } 
	  
	 @Override
	  protected void configure(HttpSecurity http) throws Exception {
		
		http.authorizeRequests()
	     .antMatchers("/").permitAll()
	     .antMatchers("/login").permitAll()
	     .antMatchers("/registerDeanOrTeacher").hasAuthority("ADMIN")
<<<<<<< HEAD
	     .antMatchers("/registerTeacher","/announcement","/postScholarship","/editScholarships","/editAnnouncements").hasAuthority("DEAN")
	     .antMatchers("/uploadPost","/listTeacherAnnouncements","/postTeacherAnnouncement","/listTeacherResources").hasAuthority("TEACHER")
=======
	     .antMatchers("/registerTeacher","/announcement").hasAuthority("DEAN")
	     .antMatchers("/uploadPost").hasAuthority("TEACHER")
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
	     				.and()
	     					.formLogin()
	     					.loginPage("/login")
	     					.failureUrl("/login?error=true")
	     					.defaultSuccessUrl("/default")
	     
		.and()
			.logout()
			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
<<<<<<< HEAD
			.logoutSuccessUrl("/")
		.and()
			.exceptionHandling()
			.accessDeniedPage("/accessDenied");
=======
			.logoutSuccessUrl("/");
		/*.and()
			.exceptionHandling()
			.accessDeniedPage("/AccessDenied");*/
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
		
		 
	  }
	 
	 @Override
	 public void configure(WebSecurity webSecurity) throws Exception {
		 
		 webSecurity.ignoring()
		 			.antMatchers("/resources/**","/static/**","/css/**","/js/**","/images/**");
		 
	 }
}
