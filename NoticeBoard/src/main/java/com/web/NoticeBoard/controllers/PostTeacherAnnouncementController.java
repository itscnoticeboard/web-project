package com.web.NoticeBoard.controllers;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.web.NoticeBoard.domains.TeacherAnnouncement;
import com.web.NoticeBoard.domains.User;
import com.web.NoticeBoard.services.TeacherAnnouncementService;
import com.web.NoticeBoard.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/postTeacherAnnouncement")
public class PostTeacherAnnouncementController {
	
	@Autowired
	UserService userService;
	@Autowired
	TeacherAnnouncementService postService;
	
	@GetMapping
	public String teacherAnnouncement(Model model,@ModelAttribute("toBeChanged") TeacherAnnouncement a) {
		if(a!=null)
			model.addAttribute("announcement",a);
		else
			model.addAttribute("announcement",new TeacherAnnouncement());
		return "postTeacherAnnouncement";
	}
	
	@PostMapping(params="post")
	public String updateAnnouncement(HttpSession session,@AuthenticationPrincipal UserDetails userDetails,@Valid @ModelAttribute("announcement") TeacherAnnouncement post,Errors errors,RedirectAttributes attribute) {
		if(errors.hasErrors()) {
			return "postTeacherAnnouncement";
		}
		User user = userService.findUserByUsername(userDetails.getUsername());
		
		ServletRequestAttributes requestAttr = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = requestAttr!=null ? requestAttr.getRequest() : null;
		
		session = request != null ? request.getSession(false) : null;
		Long id = session != null && session.getAttribute("editId") != null ? (Long)session.getAttribute("editId") : (long) 0;
		log.info("id---"+id);
	    log.info("user----"+user.getUsername());
		log.info("Resource description----"+post.getDescription()+post.getHeader()+post.getSemester()+post.getYear()+post.getCreatedAt());
		if(id!=0) {
			post.setId(id);
			post.setCreatedAt(new Date());
			session.invalidate();
		}
		
		post.setUser(user);
		
		postService.savePost(post);
		log.info("Teacher Announcement saved");
		if(id!=0)
			attribute.addFlashAttribute("success","Teacher Announcement has been updated successfully");
		else
			attribute.addFlashAttribute("success","Teacher Announcement has been added successfully");

		
		return "redirect:/postTeacherAnnouncement";
		
	}
	
	
	

}
