package com.web.NoticeBoard.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import com.web.NoticeBoard.domains.TeacherAnnouncement;
import com.web.NoticeBoard.domains.User;
import com.web.NoticeBoard.services.TeacherAnnouncementService;
import com.web.NoticeBoard.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/listTeacherAnnouncements")
public class TeacherAnnouncementController {
	@Autowired
	TeacherAnnouncementService postService;
	@Autowired
	UserService userService;
	
	@GetMapping
	public String getAnnouncements(Model model,@AuthenticationPrincipal UserDetails userDetails) {
		
		User user = userService.findUserByUsername(userDetails.getUsername());
		List<TeacherAnnouncement> posts=postService.findAnnouncementsByUser(user);
		if(posts.isEmpty()) {
			model.addAttribute("announcements","empty");
		}
		else {
		model.addAttribute("announcements",posts);
		}

		log.info("getting announcements");
		return "listTeacherAnnouncements";
	}
	
	@PostMapping(params="delete")
	public String deleteAnnouncement(@RequestParam("a_id") Long id) {
		
		postService.deletePost(id);
		return "redirect:/listTeacherAnnouncements";
	}
	@PostMapping(params="edit")
	public String editAnnouncement(@RequestParam("a_id") Long id,RedirectAttributes redirect,Model model,HttpSession session) {
		
		TeacherAnnouncement p = postService.findPostById(id).get();
		
		redirect.addFlashAttribute("toBeChanged",p);
		
		ServletRequestAttributes requestAttr=(ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		HttpServletRequest request=requestAttr.getRequest();
		session=request.getSession();
		session.setAttribute("editId", p.getId());
		
		return "redirect:/postTeacherAnnouncement";
	}

}
