package com.web.NoticeBoard.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
<<<<<<< HEAD
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
=======
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.web.NoticeBoard.domains.User;
import com.web.NoticeBoard.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/registerTeacher")
public class RegisterTeacherController {
	
  @Autowired
  private UserService userService;
  
  
  @GetMapping
  public String registerTeacher(Model model,@ModelAttribute User user,@AuthenticationPrincipal UserDetails  userDetails) {
	  
	  model.addAttribute("user",new User());
	  User user1=userService.findUserByUsername(userDetails.getUsername());
		log.info(user1.getFirstName());
      return "registerTeacher";    
  }
  
<<<<<<< HEAD
  @PostMapping(params="register")
  public String registerUser(@Valid @ModelAttribute("user") User user, Errors errors,RedirectAttributes attribute) {
	  User userExists = userService.findUserByUsername(user.getUsername());
      if (userExists != null) {
          errors
                  .rejectValue("username", "error.username",
                          "There is already a user registered with the username provided");
      }
	  if (errors.hasErrors()) {
		  return "registerTeacher";
	  }
=======
  @RequestMapping(params="register",method = RequestMethod.POST)
  public String registerUser(@Valid @ModelAttribute("user") User user, Errors errors,RedirectAttributes attribute) {
	 
	  if (errors.hasErrors()) {
		  return "registerTeacher";
	  }
	  //user.setConfirmPassword("abcdccdc");
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
	  userService.saveTeacher(user);
	  
	  log.info("Teacher saved");
	  attribute.addFlashAttribute("success","Teacher has been added successfully");

	  return "redirect:/registerTeacher";
	}
  
<<<<<<< HEAD
  
=======
  @RequestMapping(params="clear")
  public String clear() {
  	return "redirect:/registerTeacher";
  }
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
}
