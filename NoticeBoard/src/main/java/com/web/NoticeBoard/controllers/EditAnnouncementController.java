package com.web.NoticeBoard.controllers;

<<<<<<< HEAD
import java.util.List;

=======
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
<<<<<<< HEAD
import org.springframework.web.bind.annotation.RequestMapping;import org.springframework.web.bind.annotation.RequestParam;
=======
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

<<<<<<< HEAD
import com.web.NoticeBoard.domains.DeanAnnouncement;
=======
import com.web.NoticeBoard.domains.Announcement;
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
import com.web.NoticeBoard.services.AnnouncementService;
import lombok.extern.slf4j.Slf4j;



@Slf4j
@Controller
@RequestMapping("/editAnnouncements")
public class EditAnnouncementController {
		
		@Autowired
		AnnouncementService announcementService;
		
		@GetMapping
		public String getAnnouncements(Model model) {
<<<<<<< HEAD
			List<DeanAnnouncement> announcements=announcementService.findAllAnnouncements();
			if(announcements.isEmpty())
				model.addAttribute("announcements","empty");
			else
				model.addAttribute("announcements",announcements);
			log.info("getting announcements");
			return "editAnnouncements";
		}
		@PostMapping(params="delete")
=======
			model.addAttribute("announcements",announcementService.findAllAnnouncements());
			log.info("getting announcements");
			return "editAnnouncements";
		}
		@RequestMapping(params="delete",method = RequestMethod.POST)
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
		public String deleteAnnouncement(@RequestParam("a_id") Long id) {
			announcementService.deleteAnnouncement(id);
			return "redirect:/editAnnouncements";
		}
<<<<<<< HEAD
		@PostMapping(params="edit")
		public String editAnnouncement(@RequestParam("a_id") Long id,RedirectAttributes redirect,Model model,HttpSession session) {
			DeanAnnouncement a=announcementService.findAnnouncement(id).get();
=======
		@RequestMapping(params="edit",method = RequestMethod.POST)
		public String editAnnouncement(@RequestParam("a_id") Long id,RedirectAttributes redirect,Model model,HttpSession session) {
			Announcement a=announcementService.findAnnouncement(id).get();
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
			
			redirect.addFlashAttribute("toBeChanged",a);
			
			ServletRequestAttributes requestAttr=(ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
			HttpServletRequest request=requestAttr.getRequest();
			session=request.getSession();
			session.setAttribute("editId", a.getId());
			
			return "redirect:/announcement";
		}
}
