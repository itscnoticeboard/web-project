package com.web.NoticeBoard.controllers;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DefaultController {
	 @RequestMapping("/default")
	 public String DefaultPageAfterLogin(@AuthenticationPrincipal UserDetails userDetails) {
		 if(userDetails.getAuthorities().toString().equals("[ADMIN]")) {
			 return "redirect:/registerDeanOrTeacher";
		 }
		 else if(userDetails.getAuthorities().toString().equals("[DEAN]")) {
			 return "redirect:/announcement";
		 }
		 else if(userDetails.getAuthorities().toString().equals("[TEACHER]")) {
			 return "redirect:/uploadPost";
		 }
		 
		 else {
			 return "redirect:/";
		 }
	 }

}
