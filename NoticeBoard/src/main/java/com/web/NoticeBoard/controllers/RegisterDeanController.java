package com.web.NoticeBoard.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
<<<<<<< HEAD
=======
import org.springframework.ui.Model;
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
<<<<<<< HEAD
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
=======
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.web.NoticeBoard.domains.Role;
import com.web.NoticeBoard.domains.User;
import com.web.NoticeBoard.services.RoleService;
import com.web.NoticeBoard.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/registerDeanOrTeacher")
public class RegisterDeanController {
	
  @Autowired
  private UserService userService;
  @Autowired
  private RoleService roleService;
  
  @GetMapping
  public String registerDean(ModelMap model) {
	  model.addAttribute("user",new User());
	  model.addAttribute("role",new Role());
      return "registerDeanOrTeacher";    
  }
  
<<<<<<< HEAD
  @PostMapping(params="register")
  public String registerUser(@Valid @ModelAttribute("user") User user,BindingResult result_user,@Valid @ModelAttribute("role") Role role,BindingResult result_role,RedirectAttributes attribute) {
	  
	  User userExists = userService.findUserByUsername(user.getUsername());
      if (userExists != null) {
          result_user
                  .rejectValue("username", "error.username",
                          "There is already a user registered with the username provided");
      }
	  if (result_user.hasErrors() || result_role.hasErrors()) {
=======
  @RequestMapping(params="register",method = RequestMethod.POST)
  public String registerUser(@Valid @ModelAttribute("user") User user,BindingResult result_user,@Valid @ModelAttribute("role") Role role,BindingResult result_role,RedirectAttributes attribute) {
	  if (result_role.hasErrors() || result_role.hasErrors()) {
		 //log.info("P: "+user.getConfirmPassword());
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
		return "registerDeanOrTeacher";
	  }
	  
	  userService.saveUser(user);
	  role.setUser(user);
	  roleService.save(role);
	  log.info("User and role saved");
	  attribute.addFlashAttribute("success","User has been added successfully");
		
	  return "redirect:/registerDeanOrTeacher";
	}
  
 
}
