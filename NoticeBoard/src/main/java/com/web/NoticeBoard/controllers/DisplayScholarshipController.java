package com.web.NoticeBoard.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.web.NoticeBoard.domains.Scholarship;
import com.web.NoticeBoard.services.ScholarshipService;

@Controller
@RequestMapping("/displayScholarship")
public class DisplayScholarshipController {
	@Autowired
	ScholarshipService scholarshipService;
	
	@GetMapping
	public String getScholarship(Model model) {
		List<Scholarship> scholarships=scholarshipService.findAllScholarships();
		if(scholarships.isEmpty())
			model.addAttribute("scholarships", "empty");
		else
			model.addAttribute("scholarships", scholarships);
		return "/displayScholarship";
	}
}
