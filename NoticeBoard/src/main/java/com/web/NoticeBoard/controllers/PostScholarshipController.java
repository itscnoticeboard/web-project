package com.web.NoticeBoard.controllers;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import com.web.NoticeBoard.domains.Scholarship;
import com.web.NoticeBoard.domains.User;
import com.web.NoticeBoard.services.ScholarshipService;
import com.web.NoticeBoard.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping("/postScholarship")
public class PostScholarshipController {
	
	@Autowired
	UserService userService;
	@Autowired
	ScholarshipService scholarshipService;
	
	@GetMapping
	@ModelAttribute
	public String getScholarship(Model model,@ModelAttribute("toChange")Scholarship s) {
		if(s != null ) {
			model.addAttribute("scholarship",s);
		}
		
		else{
			model.addAttribute("scholarship",new Scholarship());
		}
		
		return "postScholarship";
	}
	@PostMapping(params="post")
	public String postScholarship(@Valid @ModelAttribute("scholarship") Scholarship scholarship, Errors errors,HttpSession session,@AuthenticationPrincipal UserDetails userDetails,RedirectAttributes attribute) {
			
		if(errors.hasErrors()) {
			return "postScholarship";
		}
		log.info("DeanAnnouncement: " + scholarship.getUniversityName());
		
		User user = userService.findUserByUsername(userDetails.getUsername());
		
		ServletRequestAttributes requestAttr = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = requestAttr!=null ? requestAttr.getRequest() : null;
		
		session = request !=null ? request.getSession(false) : null;
		Long id = session != null && session.getAttribute("editId") != null ? (Long)session.getAttribute("editId") : (long) 0;
		
		
		
		if(id != 0) {
			scholarship.setId(id);
			session.invalidate();
		}
		
		scholarship.setUser(user);
		scholarshipService.saveScholarship(scholarship);
		log.info("Scholarship saved");
		
		if(id!=0)
			attribute.addFlashAttribute("success","Scholarship has been updated successfully");
		else
			attribute.addFlashAttribute("success","Scholarship has been added successfully");


		return "redirect:/postScholarship";
	}

}
