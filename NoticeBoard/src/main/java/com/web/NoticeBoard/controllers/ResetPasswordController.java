package com.web.NoticeBoard.controllers;

import java.util.Locale;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.web.NoticeBoard.domains.PasswordDto;
import com.web.NoticeBoard.domains.User;
import com.web.NoticeBoard.services.UserService;

import jdk.internal.jline.internal.Log;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/resetPassword")
public class ResetPasswordController {
	@Autowired
	private UserService userService;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	
	@GetMapping
	public String resetPage(Model model) {
		model.addAttribute("pdto",new PasswordDto());
		return "resetPassword";
	}
	
	@PostMapping
	public void savePassword(Locale locale, 
	 @ModelAttribute("pdto") PasswordDto passwordDto,@AuthenticationPrincipal UserDetails userDetails) {
		log.info("here");
		User user = userService.findUserByUsername(userDetails.getUsername());
		log.info(userDetails.getUsername());
	    if(bCryptPasswordEncoder.matches(passwordDto.getOldPassword(),user.getPassword())) {
	    	userService.changePassword(user, passwordDto.getNewPassword());
	    }
	    
	   
	}
}
