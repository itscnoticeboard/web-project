package com.web.NoticeBoard.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.web.NoticeBoard.domains.Scholarship;
import com.web.NoticeBoard.services.ScholarshipService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/editScholarships")
public class EditScholarshipController {
	
	@Autowired
	ScholarshipService scholarshipService;
	
	@GetMapping
	public String getScholarship(Model model) {
		List<Scholarship> scholarships = scholarshipService.findAllScholarships();
		if(scholarships.isEmpty())
			model.addAttribute("scholarships","empty");
		else
			model.addAttribute("scholarships",scholarships);
		log.info("getting scholarships");
		return "editScholarships";
	}
	
	@PostMapping(params="delete")
	public String deleteAnnouncement(@RequestParam("s_id") Long id) {
		scholarshipService.deleteScholarship(id);
		return "redirect:/editScholarships";
	}
	
	@PostMapping(params="edit")
	public String editAnnouncement(@RequestParam("s_id") Long id,RedirectAttributes redirect,Model model,HttpSession session) {
		Scholarship s=scholarshipService.findScholarship(id).get();
		
		redirect.addFlashAttribute("toChange",s);
		
		ServletRequestAttributes requestAttr=(ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		HttpServletRequest request=requestAttr.getRequest();
		session=request.getSession();
		session.setAttribute("editId", s.getId());
		
		return "redirect:/postScholarship";
	}
}
