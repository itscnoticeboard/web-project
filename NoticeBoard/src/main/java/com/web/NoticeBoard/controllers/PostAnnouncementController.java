package com.web.NoticeBoard.controllers;



import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
<<<<<<< HEAD
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
=======
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

<<<<<<< HEAD
import com.web.NoticeBoard.domains.DeanAnnouncement;
=======
import com.web.NoticeBoard.domains.Announcement;
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
import com.web.NoticeBoard.domains.User;
import com.web.NoticeBoard.services.AnnouncementService;
import com.web.NoticeBoard.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/announcement")
public class PostAnnouncementController {
	
	@Autowired
	private AnnouncementService announcementService;
	@Autowired
	private UserService userService;
	
	@GetMapping
	@ModelAttribute
<<<<<<< HEAD
	public String announcement(Model model,@ModelAttribute("toBeChanged") DeanAnnouncement a) {
=======
	public String announcement(Model model,@ModelAttribute("toBeChanged") Announcement a,RedirectAttributes redirect) {
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
		if(a.getId() != 0) {
			model.addAttribute("announcement",a);
		}
		else {
<<<<<<< HEAD
			model.addAttribute("announcement", new DeanAnnouncement());
=======
			model.addAttribute("announcement", new Announcement());
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
		}
			return "announcement";
	}
	
	
<<<<<<< HEAD
	@PostMapping(params="post")
	public String postAnnouncement(@Valid @ModelAttribute("announcement") DeanAnnouncement announcement, Errors errors,HttpSession session,@AuthenticationPrincipal UserDetails userDetails,RedirectAttributes attribute) {
=======
	@RequestMapping(params="post",method = RequestMethod.POST)
	public String postAnnouncement(@Valid @ModelAttribute("announcement") Announcement announcement, Errors errors,HttpSession session,@AuthenticationPrincipal UserDetails userDetails,RedirectAttributes attribute) {
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
		
		if(errors.hasErrors()) {
			return "announcement";
		}
<<<<<<< HEAD
		log.info("DeanAnnouncement: "+announcement.getHeader());
		
		User user = userService.findUserByUsername(userDetails.getUsername());
		
		ServletRequestAttributes requestAttr = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = requestAttr!=null ? requestAttr.getRequest() : null;
=======
		log.info("Announcement: "+announcement.getHeader());
		User user = userService.findUserByUsername(userDetails.getUsername());
		
		ServletRequestAttributes requestAttr=(ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		HttpServletRequest request= requestAttr!=null ? requestAttr.getRequest() : null;
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
		
		session = request !=null ? request.getSession(false) : null;
		Long id = session != null && session.getAttribute("editId") != null ? (Long)session.getAttribute("editId") : (long) 0;
		
		
		
		if(id != 0) {
			
			announcement.setId(id);
			announcement.setCreatedAt(new Date());
			session.invalidate();
		}
		
		announcement.setUser(user);
		announcementService.saveAnnouncement(announcement);
<<<<<<< HEAD
		log.info("Dean Announcement saved");
		
		if(id!=0)
			attribute.addFlashAttribute("success","Announcement has been updated successfully");
		else
			attribute.addFlashAttribute("success","Announcement has been added successfully");
=======
		log.info("Announcement saved");
		attribute.addFlashAttribute("success","Post has been added successfully");
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500

		return "redirect:/announcement";
	}
	
<<<<<<< HEAD
	
=======
	@RequestMapping(params="clear")
    public String clearAnnouncement() {
    	return "redirect:/announcement";
    }
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
	
}
