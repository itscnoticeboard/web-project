package com.web.NoticeBoard.controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.web.NoticeBoard.domains.Resource;
import com.web.NoticeBoard.domains.User;
import com.web.NoticeBoard.services.PostService;
import com.web.NoticeBoard.services.UserService;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("/listTeacherResources")
@Slf4j
@Controller
public class TeacherResourceController {
	@Autowired
	PostService postService;
	@Autowired
	UserService userService;
	
	@GetMapping
	public String getResources(Model model,@AuthenticationPrincipal UserDetails userDetails) {
		
		User user = userService.findUserByUsername(userDetails.getUsername());
		List<Resource> posts=postService.findResourcesByUser(user);
		if(posts.isEmpty()) {
			model.addAttribute("announcements","empty");
		}
		else {
			model.addAttribute("announcements",posts);
		}

		log.info("getting announcements");
		return "listTeacherResources";
	}
	@PostMapping(params="delete")
	public String deleteAnnouncement(@RequestParam("a_id") Long id) {
		
		log.info("id: "+id.toString());
		
		Resource post = postService.findPostById(id).get();
	
		int f=post.getResourcePath().lastIndexOf('/');
		String folder="";
		if (f != -1) 
		{
		    folder+= post.getResourcePath().substring(0 , f); 
		}
		try {
			FileUtils.forceDelete(new File("/"+post.getResourcePath()));
			FileUtils.deleteDirectory(new File("/"+folder));
		} catch (IOException e) {
			e.printStackTrace();
		}
		postService.deletePost(id);
		
		return "redirect:/listTeacherResources";
	}

}
