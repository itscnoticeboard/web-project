package com.web.NoticeBoard.domains;

<<<<<<< HEAD
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
=======
import javax.persistence.Entity;
import javax.persistence.FetchType;
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "roles")
public class Role {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@JsonIgnore
<<<<<<< HEAD
	@OneToOne(cascade =  CascadeType.ALL, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
	private User user;
    
	@Column(name="user_role")
=======
	@OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "userId", nullable = false)
	private User user;
    
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
	@NotNull(message="Please assign a role")
    private String userRole;
}
