package com.web.NoticeBoard.domains;

import java.io.File;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "posts")
public class Post {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Date createdAt;
	
	@PrePersist
	void placedAt() {
		this.createdAt = new Date();
	}	
	
	@NotEmpty(message="Header cannot be empty")
	@NotNull
    private String header;
    
    @Column(name = "description")
    @NotEmpty(message = "Please provide the description")
    @NotNull
    private String description;
  
	
	private String resourcePath=null;
  
	@Column(name = "semester")
	private int semester;
  
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="teacher_id")
	private User user;
	
	@Transient
	private File resourceFile;
	
	@Column(name = "year")
	private String year;
	
	
}
