package com.web.NoticeBoard.domains;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
<<<<<<< HEAD
=======
import java.util.Set;
import java.util.stream.Collectors;
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
<<<<<<< HEAD
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
=======
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;
import lombok.NoArgsConstructor;
<<<<<<< HEAD
=======

>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
import com.web.NoticeBoard.domains.Role;


@Data
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User implements UserDetails{
	
<<<<<<< HEAD

	@Id
=======
    @Id
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
   
    @NotBlank(message = "Please provide a username")
    private String username;
   
    @Size(min = 8, message = "Your password must have at least 8 characters")
    @NotBlank(message = "Please provide your password")
    @NotNull(message="Password doesn't match")
<<<<<<< HEAD
    private String password; 
=======
    private String password;
    
   
    /*@NotNull(message="Password doesn't match")
    private String confirmPassword;
    
    //Setter for password
    public void setPassword(String _password) {
    	this.password=_password;
    	checkPassword();
    }
    
    public void setConfirmPassword(String _confirmPassword) {
    	this.confirmPassword=_confirmPassword;
    	checkPassword();
    }
    
    private void checkPassword() {
    	if(this.password == null || this.confirmPassword == null) 
    		return;
    	else if(!this.password.equals(this.confirmPassword)) {
    		this.confirmPassword=null;
    	}
    }*/
    
    
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
   
    @Column(name = "first_name")
    @NotBlank(message = "Please provide your first name")
    private String firstName;
    
    @Column(name = "last_name")
    @NotBlank(message = "Please provide your last name")
    private String lastName;
    
    @Email(message="Invalid Email")
    @NotBlank(message = "Please provide your email")
    private String email;
    
    @NotBlank(message="Course is required")
    private String course;
    
    @NotBlank(message="Office is required")
    private String office;
    
<<<<<<< HEAD
    @Column(name="consultation_days")
    @Size(min=1,message="You should provide a consultation day")
    private String[] consultationDays;
    
    
    @OneToOne(
=======
   @Size(min=1,message="You should provide a consultation day")
    private String[] consultationDays;
    
    
    @OneToOne(fetch = FetchType.LAZY,
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
            cascade =  CascadeType.ALL,
            mappedBy = "user")
    private Role role;
    
<<<<<<< HEAD
    
    @OneToMany(cascade=CascadeType.ALL,
    		   orphanRemoval=true,
    		   mappedBy="user")
    private List<Resource> post=new ArrayList<>();
=======
    @OneToMany(cascade=CascadeType.ALL,
    		   orphanRemoval=true,
    		   mappedBy="user")
    private List<Post> post=new ArrayList<>();
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
    
    @OneToMany(cascade=CascadeType.ALL,
 		   orphanRemoval=true,
 		   mappedBy="user")
<<<<<<< HEAD
    private List<DeanAnnouncement> announcement=new ArrayList<>();

    @OneToMany(cascade=CascadeType.ALL,
  		   orphanRemoval=true,
  		   mappedBy="user")
     private List<TeacherAnnouncement> teacherAnnouncement=new ArrayList<>();

    
=======
 private List<Announcement> announcement=new ArrayList<>();

>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {

		return Arrays.asList(new SimpleGrantedAuthority(role.getUserRole()));
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
