package com.web.NoticeBoard.services;


import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;


import com.web.NoticeBoard.domains.User;


public interface UserService extends UserDetailsService {

	User findUserByUsername(String username);
	void saveUser(User user);
	void saveTeacher(User user);
	List<User> findAllUsersByRole(String role);
	void changePassword(User user, String password);
	
	
}