package com.web.NoticeBoard.services;

import java.util.List;
import java.util.Optional;


import com.web.NoticeBoard.domains.Scholarship;

public interface ScholarshipService {
	void deleteScholarship(Long id);
	void saveScholarship(Scholarship s);
	Optional<Scholarship> findScholarship(Long id);
	List<Scholarship> findAllScholarships();
}
