package com.web.NoticeBoard.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.NoticeBoard.domains.Scholarship;
import com.web.NoticeBoard.repositories.ScholarshipRepository;

@Service
public class ScholarshipServiceImpl implements ScholarshipService {
	
	@Autowired
	ScholarshipRepository scholarshipRepo;

	@Override
	public void deleteScholarship(Long id) {
		scholarshipRepo.deleteById(id);
		
	}

	@Override
	public void saveScholarship(Scholarship s) {
		scholarshipRepo.save(s);
		
	}

	@Override
	public Optional<Scholarship> findScholarship(Long id) {
		return scholarshipRepo.findById(id);
	}

	@Override
	public List<Scholarship> findAllScholarships() {
		return (List<Scholarship>)scholarshipRepo.findAll();
	}

}
