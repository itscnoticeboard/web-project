package com.web.NoticeBoard.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.NoticeBoard.domains.TeacherAnnouncement;
import com.web.NoticeBoard.domains.User;
import com.web.NoticeBoard.repositories.TeacherRepository;

@Service
public class TeacherAnnouncementImpl implements TeacherAnnouncementService {

	@Autowired
	TeacherRepository postRepository;
	
	@Override
	public void savePost(TeacherAnnouncement p) {
		postRepository.save(p);

	}

	@Override
	public void deletePost(Long id) {
		postRepository.deleteById(id);

	}

	@Override
	public List<TeacherAnnouncement> findPostedAnnouncements(String year, String semester) {
		List<TeacherAnnouncement> teacherPosts = postRepository.findPostedAnnouncement(year, semester);
		return teacherPosts;
	}

	@Override
	public List<TeacherAnnouncement> findAnnouncementsByUser(User user) {
		List<TeacherAnnouncement> teacherPosts = postRepository.findPostedAnnouncementsByUser(user.getId());
		return teacherPosts;
	}

	@Override
	public Optional<TeacherAnnouncement> findPostById(Long id) {
		return postRepository.findById(id);
	}

}
