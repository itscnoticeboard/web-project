package com.web.NoticeBoard.services;

import java.util.List;
<<<<<<< HEAD
import java.util.Optional;

import com.web.NoticeBoard.domains.Resource;
import com.web.NoticeBoard.domains.User;

public interface PostService {
	void savePost(Resource p);
	void deletePost(Long id);
	List<Resource> findPostedAnnouncements(String year,String semester);
	List<Resource> findResourcesByUser(User user);
	List<Resource> findPostedResource(String year,String semester);
	List<Resource> findAnnouncementsByUser(User user);
	Optional<Resource> findPostById(Long id);
	
=======

import com.web.NoticeBoard.domains.Post;
import com.web.NoticeBoard.domains.User;

public interface PostService {
	void savePost(Post p);
	List<Post> findPostedAnnouncements(String year);
	List<Post> findPostedResource(String year);
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
	
}
