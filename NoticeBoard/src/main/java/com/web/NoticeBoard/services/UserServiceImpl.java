package com.web.NoticeBoard.services;



import java.util.List;

<<<<<<< HEAD

=======
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.web.NoticeBoard.domains.Role;
import com.web.NoticeBoard.domains.User;
import com.web.NoticeBoard.repositories.RoleRepository;
import com.web.NoticeBoard.repositories.UserRepository;


<<<<<<< HEAD

@Service
public class UserServiceImpl implements UserService{
	
	
	
=======
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class UserServiceImpl implements UserService{
	
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
	private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
<<<<<<< HEAD
    public UserServiceImpl(UserRepository userRepository,RoleRepository roleRepository,BCryptPasswordEncoder bCryptPasswordEncoder) {
    	this.roleRepository=roleRepository;
    	this.userRepository=userRepository;
    	this.bCryptPasswordEncoder=bCryptPasswordEncoder;
    }
   

    public User findUserByUsername(String username) {
    	return userRepository.find_By_Username(username);
=======
    public UserServiceImpl(UserRepository userRepository,
                       RoleRepository roleRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User findUserByUsername(String username) {
    	return userRepository.findByUsername(username);
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
    }
    public void saveTeacher(User user) {
    	Role r;
    	r=new Role();
    	r.setUser(user);
    	r.setUserRole("TEACHER");
    	user.setRole(r);
    	user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    	userRepository.save(user);
    	roleRepository.save(r);
    	
    }
    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
<<<<<<< HEAD
		User user = userRepository.find_By_Username(username);
=======
		User user = userRepository.findByUsername(username);
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
		
		if(user != null) {
			return user;
		}
		throw new UsernameNotFoundException("User '" + username + "' not found");
	}

	

	@Override
	public List<User> findAllUsersByRole(String role) {
		List<User> users=userRepository.findAllByRole(role);
		return users;
	}

	@Override
	public void changePassword(User user, String password) {
		
	    user.setPassword(bCryptPasswordEncoder.encode(password));
	    userRepository.save(user);
	}
}
