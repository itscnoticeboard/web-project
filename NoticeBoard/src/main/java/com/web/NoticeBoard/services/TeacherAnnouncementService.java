package com.web.NoticeBoard.services;

import java.util.List;
import java.util.Optional;


import com.web.NoticeBoard.domains.TeacherAnnouncement;
import com.web.NoticeBoard.domains.User;

public interface TeacherAnnouncementService {
	void savePost(TeacherAnnouncement p);
	void deletePost(Long id);
	List<TeacherAnnouncement> findPostedAnnouncements(String year,String semester);
	List<TeacherAnnouncement> findAnnouncementsByUser(User user);
	Optional<TeacherAnnouncement> findPostById(Long id);
	
}
