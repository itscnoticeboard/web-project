package com.web.NoticeBoard.repositories;

import org.springframework.data.repository.CrudRepository;

import com.web.NoticeBoard.domains.Scholarship;

public interface ScholarshipRepository extends CrudRepository<Scholarship, Long> {

}
