package com.web.NoticeBoard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


import com.web.NoticeBoard.domains.TeacherAnnouncement;

public interface TeacherRepository extends CrudRepository<TeacherAnnouncement, Long>  {
	@Query(value="SELECT * FROM `teacher_announcements` where `teacher_announcements`.year=:choice AND `teacher_announcements`.semester=:semester",nativeQuery=true)
	List<TeacherAnnouncement> findPostedAnnouncement(String choice,String semester);
	
	@Query(value="SELECT * FROM `teacher_announcements` where `teacher_announcements`.teacher_id=:id",nativeQuery=true)
	List<TeacherAnnouncement> findPostedAnnouncementsByUser(Long id);
	
}
