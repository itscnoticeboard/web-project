package com.web.NoticeBoard.repositories;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

<<<<<<< HEAD
import com.web.NoticeBoard.domains.Resource;

public interface PostRepository extends CrudRepository<Resource, Long> {
	
	@Query(value="SELECT * FROM resources where resources.resource_path IS NOT NULL AND resources.year=:choice AND resources.semester=:semester",nativeQuery=true)
	List<Resource> findResources(String choice, String semester);
	
	
	@Query(value="SELECT * FROM resources where posts.resource_path IS NULL AND posts.year=:choice AND posts.semester=:semester",nativeQuery=true)
	List<Resource> findPostedAnnouncement(String choice,String semester);
	
	@Query(value="SELECT * FROM posts where posts.resource_path IS NULL AND posts.teacher_id=:id",nativeQuery=true)
	List<Resource> findPostedAnnouncementsByUser(Long id);
	
	@Query(value="SELECT * FROM resources where resources.resource_path IS NOT NULL AND resources.teacher_id=:id",nativeQuery=true)
	List<Resource> findPostedResourcesByUser(Long id);
=======
import com.web.NoticeBoard.domains.Post;

public interface PostRepository extends CrudRepository<Post, Long> {
	
	@Query(value="SELECT * FROM posts where posts.resource_path IS NOT NULL AND posts.year=:choice",nativeQuery=true)
	List<Post> findResources(String choice);
	
	
	@Query(value="SELECT * FROM posts where posts.resource_path IS NULL AND posts.year=:choice",nativeQuery=true)
	List<Post> findPostedAnnouncement(String choice);
>>>>>>> 94de5792cc85a10028a61c4634c354205429d500
}
