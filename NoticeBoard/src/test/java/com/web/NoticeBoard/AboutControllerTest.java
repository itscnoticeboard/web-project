package com.web.NoticeBoard;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.web.NoticeBoard.controllers.AboutController;


@RunWith(SpringRunner.class)
@WebMvcTest(AboutController.class)
public class AboutControllerTest {
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testControllerGet() throws Exception{
		mockMvc.perform(get("/about"))
		.andExpect(status().isOk())
		.andExpect(view().name("about"))
		.andExpect(content().string(containsString("About Us")));
	}

}
