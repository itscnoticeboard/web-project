package com.web.NoticeBoard;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.web.NoticeBoard.controllers.LoginController;

@RunWith(SpringRunner.class)
@WebMvcTest(LoginController.class)
@AutoConfigureMockMvc
@ComponentScan({"com.web.NoticeBoard.services"})
@ComponentScan({"com.web.NoticeBoard.*"})
@EntityScan("com.web.NoticeBoard.domains")
@EnableJpaRepositories(basePackages="com.web.NoticeBoard.repositories")
public class LoginControllerTest {
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testControllerGet() throws Exception{
		mockMvc.perform(get("/login"))
		.andExpect(status().isOk())
		.andExpect(view().name("login"))
		.andExpect(content().string(containsString("Log In")));
	}
	
	@Test(expected = StackOverflowError.class)
	@WithMockUser(username = "admin", password = "admin")
	public void testAdminLogin() throws Exception{
		mockMvc.perform(formLogin().user("admin").password("admin"))
		.andExpect(status().isOk())
		.andExpect(view().name("registerDeanOrTeacher"))
		.andExpect(content().string(containsString("Register")));
	}
	
	@Test(expected = StackOverflowError.class)
	@WithMockUser(username = "meron123", password = "meron123")
	public void testDeanLogin() throws Exception{
		mockMvc.perform(formLogin().user("meron123").password("meron123"))
		.andExpect(status().isOk())
		.andExpect(view().name("registerTeacher"))
		.andExpect(content().string(containsString("Register")));
	}
	
	@Test(expected = StackOverflowError.class)
	@WithMockUser(username = "abe123", password = "abe12345")
	public void testTeacherLogin() throws Exception{
		mockMvc.perform(formLogin().user("abe123").password("abe12345"))
		.andExpect(status().isOk())
		.andExpect(view().name("uploadPost"))
		.andExpect(content().string(containsString("Post")));
	}
	
	
}
