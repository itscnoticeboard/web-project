package com.web.NoticeBoard;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.web.NoticeBoard.controllers.RegisterTeacherController;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;


@RunWith(SpringRunner.class)
@SpringBootTest(classes=RegisterTeacherController.class)
@AutoConfigureMockMvc
@ComponentScan({"com.web.NoticeBoard.services"})
@ComponentScan({"com.web.NoticeBoard.*"})
@EntityScan("com.web.NoticeBoard.domains")
@EnableJpaRepositories(basePackages="com.web.NoticeBoard.repositories")

public class RegisterTeacherControllerTest {
	
	
	@Autowired
	private MockMvc mockMvc;
	
	
	@Test
	public void testGetForUnloggedUser() throws Exception{
		mockMvc.perform(get("/registerTeacher"))
		.andExpect(redirectedUrl("http://localhost/login"));
	}
	
	@Test
	@WithMockUser(username = "123", password = "123")
	public void testGetForUnauthorizedUser() throws Exception{
		mockMvc.perform(formLogin().user("123").password("123"))
		.andDo(
				result -> mockMvc.perform(get("/registerTeacher"))
				.andExpect(forwardedUrl("/accessDenied")));
	}
	
	@Test(expected = StackOverflowError.class)
	@WithMockUser(username = "meron123", password = "meron123")
	public void testGetForValidUser() throws Exception{
		mockMvc.perform(formLogin().user("meron123").password("meron123"))
		.andExpect(status().isFound())
		.andDo(
				result -> mockMvc.perform(get("/registerTeacher"))
				.andExpect(status().isOk()));
	}
	

}
