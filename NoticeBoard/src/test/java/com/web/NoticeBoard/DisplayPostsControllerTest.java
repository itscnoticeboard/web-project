package com.web.NoticeBoard;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.web.NoticeBoard.controllers.DisplayPostsController;

@RunWith(SpringRunner.class)
@WebMvcTest(DisplayPostsController.class)
@AutoConfigureMockMvc
@ComponentScan({"com.web.NoticeBoard.services"})
@ComponentScan({"com.web.NoticeBoard.*"})
@EntityScan("com.web.NoticeBoard.domains")
@EnableJpaRepositories(basePackages="com.web.NoticeBoard.repositories")
public class DisplayPostsControllerTest {
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testControllerGetWithoutRequiredParam() throws Exception{
		mockMvc.perform(get("/displayPosts"))
		.andExpect(status().is4xxClientError());
		
	}
	
	@Test
	public void testControllerGetWithRequiredParam() throws Exception{
		mockMvc.perform(get("/displayPosts?year=2"))
		.andExpect(status().isOk());
	}
}
