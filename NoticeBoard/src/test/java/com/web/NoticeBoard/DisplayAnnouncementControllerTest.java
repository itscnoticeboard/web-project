package com.web.NoticeBoard;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.web.NoticeBoard.controllers.DisplayAnnouncementController;

@RunWith(SpringRunner.class)
@WebMvcTest(DisplayAnnouncementController.class)
@AutoConfigureMockMvc
@ComponentScan({"com.web.NoticeBoard.services"})
@ComponentScan({"com.web.NoticeBoard.*"})
@EntityScan("com.web.NoticeBoard.domains")
@EnableJpaRepositories(basePackages="com.web.NoticeBoard.repositories")
public class DisplayAnnouncementControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testControllerGet() throws Exception{
		mockMvc.perform(get("/displayAnnouncement"))
		.andExpect(status().isOk())
		.andExpect(view().name("displayAnnouncement"))
		.andExpect(content().string(containsString("Announcements")));
	}


}
